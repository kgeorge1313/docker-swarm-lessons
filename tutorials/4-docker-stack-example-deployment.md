# Развертывание Docker Stack

В качестве примера будет описан процесс развертывания клиентской и серверной частей приложения, выводящего `Hello World`.
Исходники: [сервер](../example/server), [клиент](../example/client).

### Создание overlay - сети
Главным отличием overlay - сетей является то, что они достуаны со всех нод Docker Swarm, поэтому при создании Docker Stack рекомендуется использовать именно их.
Необходимо создать сеть с возможостью подключения к ней новых сервисов (это делается с помощью ключа `--attachable`).

Сеть будет названа `example-services`, для ее созадния нужно ввсети команду:
```shell script
docker network create -d overlay --attachable example-services
```

Для того, чтобы запускаемые сервисы успешно подключились к сети `example-services`, добавьте в `docker-compose.yml` соответствующие пункты:

```yaml
...

services:
  server:
    ...
    networks:
      - example-services      
    ...

  client:
    ...
    networks:
      - example-services      
    ...

networks:
  example-services:
    external: true
```

Теперь сервисы смогут обращаться друг к другу не по IP адресу, а по имени сервиса.

    Например: сервис `client` хочет обратиться порту 1234 сервиса `server`. 
    Для этого он может отправить запрос по сдресу http://server:1234/ 
    
### Добавление меток нодам
Для наиболее удобного масштабирования и балансировки рекомендуется исползовать метки нод Docker Swarm. 
Для развертывания Hello World - приложения потребуется добавить метки на ноды:

Нода 1: `server-side==true`

Нода 2: `client-side==true`

После чего в `docker-compose.yml` в пункте `deploy` потребуется прописать:

```yaml
...

services:
  server:
    ...
    deploy:
      placement:
        constraints:
          - node.labels.server-side==true


  client:
    ...
    deploy:
      placement:
        constraints:
          - node.labels.client-side==true

...
```
Таким образом будет установлено, что клиентская чать будет запускаться исключительно на ноде c меткой `client-side==true` (нода 2), а серверная часть будет запускаться на на ноде c меткой `server-side==true` (нода 1). 

### Сборка образов контейнеров

Для сборки образов необходимо скопировать папку c проектом example на одну из нод Docker Swarm, после чего необходимо перейти в нее и произвести следующие действия:

Для сборки образов ввести команду

`
docker-compose build
`

После этого докер соберет образы и вам понадобится выгрузить их в Docker Registry командой:

`
docker-compose push
`


Готово. Образы собраны и выгружены в Docker Registry. 

Теперь любая ранее сконфигурированная нода может создавать контенейры и сервисы из этих образов.

### Развертывание

Для развертывания Docker Stack потребуется ввести одну команду:

`
docker stack deploy -c docker-compose.yml example
`

После этого Докер возьмет заранее загруженный из Registry образ (или загрузит его перед развертыванием) и развернет в Docker Swarm.


### Использование

В файле `docker-compose.yml`, в конфигурации сервиса `server` прописано: 

```yaml
...
    ports:
      - 1234:1234
...
```
Это означает, что порт 1234 сервиса `server` будет доступен даже через IP адрес хоста (ноды, на котором запускается этот сервис). В данном случае это нода 1.

Можно перети по адресу http://<node_1_ip>:1234/ и увидеть надпись `Hello World!`