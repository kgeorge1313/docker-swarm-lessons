### Введение

Doocker Registry это репозиторий для Docker. 
Запускается в Docker контейнере и работает в качестве централизованного хранилища Docker образов.
Блягодаря ему можно собрать образ Docker - контейнера лишь один раз, после чего отправить его в Docker Registry и скачать его на других хостах.

### Развертывание Docker Registry

Docker Registry будет развернут на ноде 2. 
Чтобы Docker Swarm понял на какой именно ноде развертывать Docker Registry, нужно дать ноде 2 метку `registry=true`. В параметрах при развертывании контейнера нужно указать `--constraint node.labels.registry==true`, так Docker поймет что контейнер нужно будет развернуть именно на ноде с меткой `registry=true`.

1. Создадим директорию `/mnt/registry`

2. Вводим команду:

```
docker run -d \
  -p 5000:5000 \
  --restart=always \
  --name registry \
  --constraint node.labels.registry==true \
  -v /mnt/registry:/var/lib/registry \
  registry:2
```

Docker сам найдет и скачает образ registry, после чего развернет сервис.

3. Для удобства использования пропишем адрес registry в файле `/etc/hosts` (на всех нодах Docker Swarm)

```
<node2_ip> docker-registry
```

### Использование Docker Registry

1. Загрузка образа python:3.7

```
docker pull python:3.7
```

2. Смена имени образа
```
docker tag python:3.7 docker-registry/python:3.7
```

3. Выгрузка в Docker Registry
```
docker push docker-registry/python:3.7
```

Теперь образ `docker-registry/python:3.7` доступен со всемх нод, имеющих доступ к docker-registry.